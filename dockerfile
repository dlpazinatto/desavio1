FROM node:16.15.0-alpine3.15
LABEL MAINTAINER="Dilceu Pazinatto <dlpazinatto@gmail.com>"
LABEL APP_VERSION="1.2.8"
WORKDIR /src/app
RUN apk --no-cache add git
RUN git clone https://gitlab.com/dlpazinatto/api-testes.git api
WORKDIR /src/app/api
RUN npm install -g typescript
RUN npm install
RUN npm run build
EXPOSE 9010
CMD [ "node","dist/server.js" ]